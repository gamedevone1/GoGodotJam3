extends Control

onready var TextPrompt = $Margin/TextPrompt
onready var PageFlipSFX = $PageFlipSFX
onready var KeyPressSFX = $KeyPressSFX

var CurrLetterIndex: int = 0

var TypedEntriesCount: int = 0
var ErrorEntryCount: int = 0


func _ready():
	TextPrompt.Update(CurrLetterIndex)

func _unhandled_input(event):
	var KeyTyped: String = ""
	
	if event is InputEventKey and event.is_pressed() and not event.is_echo():
		KeyTyped = PoolByteArray([event.unicode]).get_string_from_utf8()
		
		var Prompt = TextPrompt.bbcode_text
		var NxtChar = Prompt.substr(CurrLetterIndex, 1)
		
		if KeyTyped == NxtChar:
			#print("You have successfully typed %s" % NxtChar)
			CurrLetterIndex += 1
			TextPrompt.Update(CurrLetterIndex)
			
			TypedEntriesCount += 1
			KeyPressSFX.play()
			
			if CurrLetterIndex == Prompt.length():
				CurrLetterIndex = 0
				ErrorEntryCount = 0
				TextPrompt.NextPassage()
				PageFlipSFX.play()
				
		else:
			ErrorEntryCount += 1
			TextPrompt.Update(CurrLetterIndex, true)
			print("You have incorrectly typed %s" % NxtChar)
			#print("Letter typed %s" % KeyTyped)
			
		NxtChar = Prompt.substr(CurrLetterIndex, 1)
		if NxtChar == '\n':
			CurrLetterIndex += 1
			TextPrompt.Update(CurrLetterIndex)
