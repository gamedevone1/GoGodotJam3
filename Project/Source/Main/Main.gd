extends Node

# Player State
var HasPlayerStarted: bool = false

# Time Tracking
var TimeStart: float = 0.0
var TimeNow: float = 0.0
var TimeElasped: float = 0.0

# Word Tracking
var TypedEntriesCount: int = 0
var ErrorEntryCount: int = 0
var ExpectedWPM: float = 40.0
var SpeedRatio: float = 0.0
onready var WordBlob: Control = $VBoxContainer/WordBlob

# Lights
const MAX_LIGHT_SIZE: int = 10
onready var WorldLight: Light2D = $WorldLight
onready var WorldLightTween: Tween = $WorldLight/Tween


func _ready():
	set_process(false)
	
func _unhandled_input(event):
	# Do not start the game until the player has pressed a key
	if not HasPlayerStarted and event is InputEventKey:
		TimeStart = OS.get_ticks_msec()
		set_process(true)
		HasPlayerStarted = true
	
func _process(delta):
	
	# Get time elasped in milliseconds
	TimeNow = OS.get_ticks_msec()
	TimeElasped = TimeNow - TimeStart
	
	# Wait for next frame if no time has passed
	if TimeElasped == 0:
		return
	
	# Convert to seconds
	TimeElasped /= 60000
	
	# Get total number of entries typed 
	# Minimum 5 letters per correct key press
	TypedEntriesCount = WordBlob.TypedEntriesCount / 5
	ErrorEntryCount = WordBlob.ErrorEntryCount
	
	# Calculate Words Per Minute
	var GrossWPM = TypedEntriesCount /  TimeElasped
	var EPM = ErrorEntryCount / TimeElasped
	
	# Detemine the rate at which the light should grow
	# Based off the ratio between Actual and Expected WPM
	SpeedRatio =  clamp((GrossWPM / ExpectedWPM) * MAX_LIGHT_SIZE,
						0, 
						MAX_LIGHT_SIZE)
	#var InverseSpeedRatio = 1 - (GrossWPM / ExpectedWPM)
	
	if SpeedRatio > 0:
		WorldLightTween.interpolate_property(WorldLight,
												"texture_scale", 
												WorldLight.texture_scale, 
												SpeedRatio,
												0.01,
												Tween.TRANS_CUBIC,
												Tween.EASE_IN)
		WorldLightTween.start()
	
	# Restart the game if player types too slow
	if SpeedRatio < 0.5 and SpeedRatio > 0:
		WordBlob.set_process_input(false)
		get_tree().reload_current_scene()
