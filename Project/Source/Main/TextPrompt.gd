extends RichTextLabel

const GreenBBCodeStartTag = "[color=green]"
const YellowBBCodeStartTag = "[color=yellow]"
const RedBBCodeStartTag = "[color=red]"
const WhiteBBCodeStartTag = "[color=white]"
const ColorBBCodeEndTag = "[/color]"



onready var StoryPassages: Array = $Story.Passages

var CurrPassageIndex: int = -1

func _ready():
	NextPassage()


func Update(index: int, isLetterIncorrect: bool = false):
	var Cursor = "[ghost]" + "|" + "[/ghost]"
	
	if isLetterIncorrect:
		Cursor = RedBBCodeStartTag + Cursor + ColorBBCodeEndTag
		
	var GreenText: String = GreenBBCodeStartTag + bbcode_text.substr(0, index) + ColorBBCodeEndTag
	var YellowText: String = YellowBBCodeStartTag + Cursor + bbcode_text.substr(index, 1) + ColorBBCodeEndTag
	var WhiteText: String = WhiteBBCodeStartTag + bbcode_text.substr(index + 1, bbcode_text.length()) + ColorBBCodeEndTag
	
	var FinalText = ""
	if index == 0:
		FinalText = YellowText + WhiteText
	else:
		FinalText = GreenText + YellowText + WhiteText
		
	parse_bbcode(FinalText)


func NextPassage():
	CurrPassageIndex += 1
	if CurrPassageIndex < StoryPassages.size():
		bbcode_text = StoryPassages[CurrPassageIndex]
		Update(0)
