extends CPUParticles2D

signal finished

func MaxSize() -> float:
	return scale_amount_curve.max_value

func GetGlowRingSize() -> float:
	return scale_amount_curve.get_point_position(1).y
	
func SetGlowRingSize(size: float):
		size = clamp(size, 
						scale_amount_curve.min_value,
						scale_amount_curve.max_value)
		scale_amount_curve.set_point_value(1,size)
	

func GetGlowRingRate() -> float:
	return lifetime
	
func SetGlowRingRate(speed: float):
	lifetime = speed

func _ready():
	one_shot = true
	emitting = true
	var time = lifetime * (1.5 - explosiveness)
	get_tree().create_timer(time, false).connect("timeout", self, "emit_signal", ["finished"])
	



func _on_GlowRing_finished():
	queue_free()
